package com.pnord.coupedumonde.equipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pnord.coupedumonde.joueurs.Joueur;
import com.pnord.coupedumonde.poule.Poule;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

/**
 * Entité JPA
 * Objet qui sert à mapper une table et les colonnes de la base de données, ici table equipes
 */
@Entity
@Table(name = "equipes")
public class Equipe {

    @Id
    private long id;

    @NotBlank
    @Size(min = 3, max = 20)
    private String nom;

    @Column(name="code")
    private String code;

    @OneToMany(mappedBy="equipe", fetch = FetchType.EAGER)
    private List<Joueur> joueurs;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id_poule")
    Poule poule;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(List<Joueur> joueurs) {
        this.joueurs = joueurs;
    }

    public Poule getPoule() {
        return poule;
    }

    public void setPoule(Poule poule) {
        this.poule = poule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equipe equipe = (Equipe) o;
        return id == equipe.id &&
                Objects.equals(nom, equipe.nom) &&
                Objects.equals(code, equipe.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, code);
    }

    @Override
    public String toString() {
        return "Equipe{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
