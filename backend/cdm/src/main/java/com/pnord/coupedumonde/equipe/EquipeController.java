package com.pnord.coupedumonde.equipe;

import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Controller Rest
 * Permet d'exposer un ensemble d'operations sur la resource Equipes, sous forme d'api REST, accessible via des urls HTTP
 * La donnée est sérializée au format JSON
 */
@RestController
@RequestMapping("/equipes")
@Api(value = "Equipes", tags = { "Equipes" })
@Transactional
public class EquipeController {

    private EquipeService equipeService;

    public EquipeController(EquipeService equipeService) {
        this.equipeService = equipeService;
    }

    /**
     * ex: GET http://localhost:8888/api/equipes
     *
     * Recupère toutes les équipes
     * @return une liste d'equipes au format json
     */
    @GetMapping
    public ResponseEntity<List<Equipe>> getEquipes() {

        List<Equipe> equipes = equipeService.getEquipes();

        return ResponseEntity.ok(equipes);
    }

    /**
     * ex: GET http://localhost:8888/api/equipes/8
     * Récupère une équipe par son id
     * @return une equipe
     */
    @GetMapping("/{id}")
    public ResponseEntity<Equipe> fetchById(@PathVariable("id") long id) {

        Optional<Equipe> optionalEquipe = equipeService.getEquipeById(id);

        return optionalEquipe.isPresent() ? ResponseEntity.ok(optionalEquipe.get()) : ResponseEntity.notFound().build();
    }

    /**
     * ex: POST http://localhost:8888/api/equipes + objet equipe au format json
     * Ajoute une equipe
     * @return l'equipe nouvellement créée
     */
    @PostMapping
    public ResponseEntity<Equipe> createEquipe(@Valid @RequestBody Equipe equipe) {

        equipeService.addEquipe(equipe);

        return ResponseEntity.ok(equipe);
    }


    /**
     * ex: PUT http://localhost:8888/api/equipes8 + objet equipe au format json
     * Mets à jour une equipe via son id
     * @return objet equipe mise a jour
     */
    @PutMapping("/{id}")
    public ResponseEntity<Equipe> updateEquipeById(@Valid @RequestBody Equipe equipe) {

        equipeService.updateEquipe(equipe);

        return ResponseEntity.ok(equipe);
    }

    /**
     * ex: DELETE http://localhost:8888/api/equipes8 + objet equipe au format json
     * Supprime une équipe par son id
     * @param id long
     * @return void, Response HTTP 200
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEquipeById(@PathVariable("id") long id) {

         equipeService.deleteById(id);

        return ResponseEntity.ok().build();
    }
}
