package com.pnord.coupedumonde.equipe;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

    /**
     * Interface Spring de Type Repository
     * Propose un ensemble de méthodes permettant d'accéder aux données Equipes
     * Couche d'abstraction facilitant la communication avec la base de données en proposant des méthodes CRUD
     */
public interface EquipeRepository extends JpaRepository<Equipe, Long> {

    Optional<Equipe> findByNomAndCode(String nom, String code);

    Optional<CodeOnly> findAllByCode(String code);
}
