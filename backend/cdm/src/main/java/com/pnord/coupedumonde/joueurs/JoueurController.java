package com.pnord.coupedumonde.joueurs;

import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller Rest
 * Permet d'exposer un ensemble d'operations sur la resource Joueurs, sous forme d'api REST, accessible via des urls HTTP
 * La donnée est sérializée au format JSON
 */
@RestController
@RequestMapping("/joueurs")
@Api(value = "Joueurs", tags = {"Joueurs"})
public class JoueurController {

    private JoueurRepository joueurRepository;

    public JoueurController(JoueurRepository joueurRepository) {
        this.joueurRepository = joueurRepository;
    }

    /**
     * Recupère la liste de tous les joueurs d'une equipe
     * Si id = 0, la liste de tous les joueurs est renvoyée
     */
    @GetMapping
    public ResponseEntity<List<Joueur>> getJoueursParEquipe(@RequestParam("equipe") long id) {

        List<Joueur> joueurs = (id == 0) ? joueurRepository.findAll() : joueurRepository.findByEquipeId(id);
        return ResponseEntity.ok(joueurs);
    }
}