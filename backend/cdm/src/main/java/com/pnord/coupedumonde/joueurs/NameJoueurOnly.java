package com.pnord.coupedumonde.joueurs;

import org.springframework.beans.factory.annotation.Value;

/**
 * Projection spring basée sur une interface
 * Elle permet de customiser dynamiquement la réponse que l'on souhaite renvoyer au frontend
 * Ici le nom complet
 */
public interface NameJoueurOnly {

    @Value("#{target.prenom + ' ' + target.nom}")
    String getFullName();
}
