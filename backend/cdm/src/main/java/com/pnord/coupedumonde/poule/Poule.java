package com.pnord.coupedumonde.poule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pnord.coupedumonde.equipe.Equipe;
import com.pnord.coupedumonde.joueurs.Joueur;
import com.pnord.coupedumonde.joueurs.Poste;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Entité JPA
 * Objet qui sert à mapper une table et les colonnes de la base de données, ici table joueurs
 */
@Entity
@Table(name="poules")
public class Poule {

    @Id
    private long id;

    @NotNull
    @Size(min = 3, max = 20)
    private String nom;

    @OneToMany(mappedBy="poule", fetch = FetchType.EAGER)
    List<Equipe> equipes;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(List<Equipe> equipes) {
        this.equipes = equipes;
    }

    @Override
    public String toString() {
        return "Poule{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
