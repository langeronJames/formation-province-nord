package com.pnord.coupedumonde.poule;

import com.pnord.coupedumonde.equipe.Equipe;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller Rest
 * Permet d'exposer un ensemble d'operations sur la resource Poules, sous forme d'api REST, accessible via des urls HTTP
 * La donnée est sérializée au format JSON
 */
@RestController
@RequestMapping("/poules")
@Api(value = "Poules", tags = { "Poules" })
@Transactional
public class PouleController {

    private PouleRepository pouleRepository;

    public PouleController(PouleRepository pouleRepository) {
        this.pouleRepository = pouleRepository;
    }

    /**
     * ex: GET http://localhost:8888/api/poules
     *
     * Recupère toutes les équipes
     * @return une liste d'equipes au format json
     */
    @GetMapping
    public ResponseEntity<List<Poule>> getPoules() {

        List<Poule> poules = pouleRepository.findAll();

        return ResponseEntity.ok(poules);
    }

    /**
     * ex: GET http://localhost:8888/api/poules/8
     * Récupère une équipe par son id
     * @return une equipe
     */
    @GetMapping("/{id}")
    public ResponseEntity<Poule> fetchById(@PathVariable("id") long id) {

        Optional<Poule> optionalPoule = pouleRepository.findById(id);

        return optionalPoule.isPresent() ? ResponseEntity.ok(optionalPoule.get()) : ResponseEntity.notFound().build();
    }

}
