
-- Drop table

-- DROP TABLE public.poules;

CREATE TABLE public.poules (
	id int8 NOT NULL,
	nom varchar NOT NULL,
	CONSTRAINT poules_pk PRIMARY KEY (id),
	CONSTRAINT poules_un UNIQUE (nom)
);


INSERT INTO public.poules (id,nom) VALUES
(1,'POULE A')
,(2,'POULE B')
,(3,'POULE C')
,(4,'POULE D')
;