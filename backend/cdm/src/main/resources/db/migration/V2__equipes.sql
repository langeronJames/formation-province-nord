CREATE TABLE public.equipes (
	id int8 NOT NULL,
	nom varchar NOT NULL,
	code varchar NOT NULL,
	id_poule int8 NOT NULL DEFAULT 1,
	CONSTRAINT equipes_pk PRIMARY KEY (id),
	CONSTRAINT equipes_un_code UNIQUE (code),
	CONSTRAINT equipes_un_nom UNIQUE (nom),
	CONSTRAINT equipes_fk FOREIGN KEY (id_poule) REFERENCES poules(id)
);

INSERT INTO public.equipes (id,nom,code,id_poule) VALUES
(7,'Ecosse','SCO',1)
,(8,'Japon','JPN',1)
,(10,'Irlande','IRE',1)
,(12,'Samoa','SAM',1)
,(17,'Russie','RUS',1)
,(1,'Nouvelle-Zélande','NZL',2)
,(2,'Angleterre','ENG',3)
,(3,'Fidji','FIJ',4)
,(4,'Australie','AUS',4)
,(11,'Géorgie','GEO',4)
;
INSERT INTO public.equipes (id,nom,code,id_poule) VALUES
(13,'Uruguay','URU',4)
,(6,'Pays de Galles','WAL',4)
,(5,'Argentine','ARG',3)
,(9,'France','FR',3)
,(14,'Tonga','TGA',3)
,(16,'USA','USA',3)
,(15,'Namibie','NAM',2)
,(18,'Afrique du Sud','RSA',2)
,(19,'Italie','ITA',2)
,(20,'Canada','CAN',2)
;