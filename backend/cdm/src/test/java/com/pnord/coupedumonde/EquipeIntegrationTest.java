package com.pnord.coupedumonde;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@Rollback
public class EquipeIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldBeOkWhenGetEquipes() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                .get("/equipes")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Ignore
    @Test
    public void shouldBeOkWhenPost() throws Exception {
        this.mockMvc.perform(post("/equipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":9999,\"nom\":\"westeros\", \"code\":\"WES\", \"poule\":{\"id\":9999} }"))
                .andExpect(status().isOk());

    }

    @Test
    public void shouldBeKOWhenIdIsWrong() throws Exception {
        mockMvc.perform( MockMvcRequestBuilders
                .get("/equipes/10000000")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}