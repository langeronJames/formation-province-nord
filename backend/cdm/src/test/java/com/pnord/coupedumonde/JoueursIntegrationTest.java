package com.pnord.coupedumonde;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class JoueursIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldBeOKWhenCallGetJoueurs() throws Exception {
		mockMvc.perform( MockMvcRequestBuilders
				.get("/equipes")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
}