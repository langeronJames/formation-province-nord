import { Component, OnInit } from "@angular/core";
import { PouleService } from "./poules.service";

@Component({
  selector: "app-poules",
  templateUrl: "./poules.component.html",
  styleUrls: ["./poules.component.scss"]
})
export class PoulesComponent implements OnInit {

  poules: any = [];

  constructor(private poulesSvc: PouleService) {}

  ngOnInit() {
    this.poulesSvc.fetchPoules().subscribe(res => this.poules = res);
  }
}
