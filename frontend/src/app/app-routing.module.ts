import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TokenGuard } from './shared/token.guard';

const routes: Routes = [
   { path: 'poules', loadChildren: './poules/poules.module#PoulesModule'},
   { path: 'equipes', loadChildren: './equipes/equipes.module#EquipesModule'},
   { path: 'joueurs', loadChildren: './joueurs/joueurs.module#JoueursModule',    canActivate: [TokenGuard]},
  {
    path: '',
    redirectTo: '/poules',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
