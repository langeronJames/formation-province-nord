import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../equipe.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.component.html',
  styleUrls: ['./equipe.component.scss']
})
export class EquipeComponent implements OnInit {

  equipe: any;
  
  constructor(private route: ActivatedRoute, private equipeSvc: EquipeService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.equipeSvc.fetchEquipe(params['id']).subscribe(res => this.equipe = res);
    });
  }
}
