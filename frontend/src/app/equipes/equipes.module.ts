import { NgModule } from '@angular/core';
import { EquipesComponent } from './equipes.component';
import { Routes, RouterModule } from '@angular/router';
import { EquipeComponent } from './equipe/equipe.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: '', component: EquipesComponent },
  { path: ':id', component: EquipeComponent }
];

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(routes)
  ],
  declarations: [EquipesComponent, EquipeComponent],
  exports: [EquipesComponent, EquipeComponent],
  providers: []
})
export class EquipesModule { }
